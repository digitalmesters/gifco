Quidco GIF API Tech Task.
=========================

`composer install`

Create the .env file and add

`GIF_SERVICE_API_KEY=<api key>`

api key is in the email ...

`./artisan serve`

Here are some good searches to get you going:

* head
* run
* coffee

Unit Tests
----------

`artisan test`

Laravel Contracts Vs Facades
----------------------------

Because I chose to create the GifService as something that could potentially be used
outside of the laravel app I couldn't used facades as that would create a dependency
on the Laravel framework. Contracts on the other hand are just class interfaces, and
while it's a bit more work to wire them in to the Laravel framework, it's not that
hard, they are bound in the Providers/AppServiceProvider class. You also get a little
more control over the tests.

Code Coverage
-------------

I wouldn't normally include code coverage in a source code repo, but it shows that the
tests I've written cover everything they need to.

OO / Dependency Injection
-------------------------

The code uses an interface to define the contract the gif service needs to implement
and the `FrontController::show` method uses type hinting to allow Laravel's DI to work
out which classes need to be instantiated and passed to this method.

The `GifService` class as required by the `show` method takes an extra parameter that
cannot by type hinted by the DI container because it's a string. `show` also only type
hints the contract, not the concrete class that implements it. Both of these are wired
up in Laravel's `AppServiceProvider` and this is where you could swap out for a different
class that also implements the interface.

I also use dependency injection in the unit tests for the GifService, passing in a Guzzle
client pre-configured to return a data set that I control so that it doesn't actually
contact giphy when the tests are run.

I've also implemented some Http tests to quickly test the front end, it makes sure that
text and error messages are present when expected

Comments
--------

I try and keep my comments short and to the point, if I find myself explaining what's going
on then it usually means what I'm doing is too complicated and needs simplifying, especially
for inline comments. A good example though is in `testSearchThrowsOnInvalidJson` where I
point out what's wrong with the JSON because it wouldn't be easy to spot otherwise. I also
add the relevant decorators to methods & classes, though these can change depending on
individual companies coding standards.