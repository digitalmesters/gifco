<?php

namespace App\Providers;

use DigitalMesters\Services\GifService;
use DigitalMesters\Contracts\GifServiceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        ClientInterface::class => Client::class
    ];

    public function boot()
    {
        $this->app->bind(GifServiceInterface::class, function ($app) {
            return new GifService($app->get(ClientInterface::class), env('GIF_SERVICE_API_KEY'));
        });
    }
}
