<?php

namespace DigitalMesters\Services;

use DigitalMesters\Contracts\GifServiceInterface;
use GuzzleHttp\Exception\BadResponseException;
use JsonException;

class GifService implements GifServiceInterface
{
    // These class constants should be pulled out into config for a production system.
    const API_URL = 'https://api.giphy.com/v1/gifs/search';

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /** @var string */
    private $apiKey;

    /**
     * @param \GuzzleHttp\ClientInterface $client
     * @param [type] $apiKey
     */
    public function __construct(\GuzzleHttp\ClientInterface $client, $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    /**
     * Polls the gif service and returns 5 results for the search term.
     *
     * @param string $term
     * @throws JsonException
     * @return object
     */
    public function search(string $term)
    {
        try {
            $res = $this->client->request('GET', self::API_URL, [
                'query' =>
                [
                    'api_key' => $this->apiKey,
                    'q' => $term,
                    'limit' => 5
                ]
            ]);
        } catch (BadResponseException $e) {
            return (object) [
                'error' => $e->getMessage()
            ];
        }

        return json_decode($res->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
    }
}
