<?php

namespace App\Http\Controllers;

use DigitalMesters\Contracts\GifServiceInterface;
use Illuminate\Http\Request;
use JsonException;

class FrontController extends Controller
{
    public function show(Request $request, GifServiceInterface $gifService)
    {
        $errors = [];
        $validatedData = $request->validate([
            'searchTerm' => ['max:12']
        ]);

        $templateData = [
            'searchTerm' => $request->input('searchTerm')
        ];

        try {
            $result = $gifService->search($validatedData['searchTerm']);

            if (!isset($result->error)) {
                $templateData['imageUrl'] = $result->data[0]->images->original->url;
            } else {
                $errors[] = "Sorry, you cannot have your gif fix right now, please try later.";
            }
        } catch (JsonException $e) {
            $imageUrl = '';
        }

        return view('default', $templateData)->withErrors($errors);
    }
}
