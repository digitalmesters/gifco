<?php

namespace DigitalMesters\Contracts;

interface GifServiceInterface
{
    public function search(string $term);
}
