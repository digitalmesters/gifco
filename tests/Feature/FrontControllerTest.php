<?php

namespace Tests\Feature;

use DigitalMesters\Contracts\GifServiceInterface;
use DigitalMesters\Services\GifService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class FrontControllerTest extends TestCase
{
    /**
     * @test
     * @covers \App\Http\Controllers\FrontController
     */
    public function testSearchFormIsRenderedByDefault()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('Show me a gif for');
        $response->assertSee('<button type="submit">Search</button>', false);

        // Don't forget the CSRF token.
        $response->assertSee('<input type="hidden" name="_token"', false);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\FrontController
     */
    public function testSearchResults()
    {
        $mockHandler = new MockHandler([
            new Response(200, [], '{
                "data": [
                  {
                    "images": {
                        "original": {
                            "url": "https://media.giphy.com/media/yvBAuESRTsETqNFlEl/giphy.gif"
                        }
                    }
                  }
                ],
                "pagination": {},
                "meta": {}
              }')
        ]);

        $handlerStack = HandlerStack::create($mockHandler);

        // uses a Guzzle client with mocked responses, inject this instead of the one provided by the client.
        $client = new Client(['handler' => $handlerStack]);
        $searchService = new GifService($client, '');

        $this->instance(GifServiceInterface::class, $searchService);

        $response = $this->get('/search?searchTerm=facepalm');
        $response->assertStatus(200);
        $response->assertViewIs('default');
        $response->assertSee('https://media.giphy.com/media/yvBAuESRTsETqNFlEl/giphy.gif');
    }

    /**
     * @test
     * @covers \App\Http\Controllers\FrontController
     */
    public function testSearchPhraseMustBe12CharsOrLess()
    {
        $response = $this->get('/search?searchTerm=This+search+phrase+is+longer+than+it+should+be+by+some+margin');

        $response->assertStatus(302);
        $response->assertSessionHasErrors(['searchTerm' => 'The search term may not be greater than 12 characters.']);
    }

    /** @test */
    public function testInvalidJsonResultDoesntShowToUser()
    {
        // The JSON is missing a closing } on "original" to make it invalid.
        $mockHandler = new MockHandler([
            new Response(200, [], '{
                "data": [
                  {
                    "images": {
                        "original": {
                            "url": "https://media.giphy.com/media/yvBAuESRTsETqNFlEl/giphy.gif"s
                    }
                  }
                ],
                "pagination": {},
                "meta": {}
              }')
        ]);

        $handlerStack = HandlerStack::create($mockHandler);

        // uses a Guzzle client with mocked responses, inject this instead of the one provided by the client.
        $client = new Client(['handler' => $handlerStack]);
        $searchService = new GifService($client, '');

        $this->instance(GifServiceInterface::class, $searchService);

        $response = $this->get('/search?searchTerm=facepalm');

        $response->assertStatus(200);
        $response->assertViewIs('default');
        $response->assertDontSee('https://media.giphy.com/media/yvBAuESRTsETqNFlEl/giphy.gif');
    }

    public function testAPIErrorDoesntShowToUser()
    {
        // The JSON is missing a closing } on "original" to make it invalid.
        $mockHandler = new MockHandler([
            new Response(500, [], 'I am Error')
        ]);

        $handlerStack = HandlerStack::create($mockHandler);

        // uses a Guzzle client with mocked responses, inject this instead of the one provided by the client.
        $client = new Client(['handler' => $handlerStack]);
        $searchService = new GifService($client, '');

        $this->instance(GifServiceInterface::class, $searchService);

        $response = $this->get('/search?searchTerm=facepalm');
        $response->assertStatus(200);
        $response->assertViewIs('default');
        $response->assertSee("Sorry, you cannot have your gif fix right now, please try later.");
    }
}
