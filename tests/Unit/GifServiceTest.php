<?php

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

use PHPUnit\Framework\TestCase;

use DigitalMesters\Services\GifService;

/**
 * GifServiceTest
 * @group group
 */
class GifServiceTest extends TestCase
{
    /**
     * @test
     * @covers \DigitalMesters\Services\GifService
     */
    public function testSearchServiceSearchMethod()
    {
        $mockHandler = new MockHandler([
            new Response(200, [], '{
                "data": [
                    {
                      "images": {
                          "original": {
                              "url": "https://media.giphy.com/media/yvBAuESRTsETqNFlEl/giphy.gif"
                          }
                      }
                    }
                  ],
                "pagination": {},
                "meta": {}
              }')
        ]);

        $handlerStack = HandlerStack::create($mockHandler);

        // uses a Guzzle client with mocked responses, inject this instead of the one provided by the client.
        $client = new Client(['handler' => $handlerStack]);
        $searchService = new GifService($client, '');

        $result = $searchService->search('kitty');

        $this->assertEquals(
            "https://media.giphy.com/media/yvBAuESRTsETqNFlEl/giphy.gif",
            $result->data[0]->images->original->url
        );
    }

    /** @test */
    public function testSearchThrowsOnInvalidJson()
    {
        // The JSON here is not valid, it's missing a closing } on "original"
        $mockHandler = new MockHandler([
            new Response(200, [], '{
                "data": [
                    {
                      "images": {
                          "original": {
                              "url": "https://media.giphy.com/media/yvBAuESRTsETqNFlEl/giphy.gif"
                      }
                    }
                  ],
                "pagination": {},
                "meta": {}
              }')
        ]);

        $handlerStack = HandlerStack::create($mockHandler);

        // uses a Guzzle client with mocked responses, inject this instead of the one provided by the client.
        $client = new Client(['handler' => $handlerStack]);
        $searchService = new GifService($client, '');

        $this->expectException(JsonException::class);

        $searchService->search('kitty');
    }

    /** @test */
    public function testAPIFailuesReturnErrorObject()
    {
        $mockHandler = new MockHandler([
            new Response(500, [], 'Some error message I guess.')
        ]);

        $handlerStack = HandlerStack::create($mockHandler);

        // uses a Guzzle client with mocked responses, inject this instead of the one provided by the client.
        $client = new Client(['handler' => $handlerStack]);
        $searchService = new GifService($client, '');

        $result = $searchService->search('kitty');

        $this->assertEquals("Server error: `GET https://api.giphy.com/v1/gifs/search?api_key=&q=kitty&limit=5` resulted in a `500 Internal Server Error` response:\nSome error message I guess.\n", $result->error);
    }
}
